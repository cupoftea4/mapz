﻿using UnityEngine;

public interface IRangeDamage
{
    public void Shoot(Vector3 targetPosition);
}
