using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    void Start()
    {
        // Start the countdown to destroy the bullet
        StartCoroutine(DestroyAfterDelay(gameObject, .5f));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Get the enemy component from the collision
        var enemy = collision.gameObject.GetComponent<Enemy>();
        var damageBonus = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().DamageBonus;
        if (enemy == null) return;
        enemy.TakeDamage(1 + damageBonus);
        Destroy(gameObject);
    }

    protected IEnumerator DestroyAfterDelay(GameObject obj, float delay)
    {
        // Wait for the specified delay
        yield return new WaitForSeconds(delay);

        // Destroy the bullet game object
        Destroy(obj);
    }
}