﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class BulletFactory : MonoBehaviour
{
    
    
    [SerializeField] public int bulletForce = 20;
    [SerializeField] protected Transform attackPoint;
    
    
    public abstract GameObject CreateBullet(Vector3 direction);

}