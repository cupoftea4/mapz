using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] public float speed = 5f;
    private Vector2 _movement;
    private Vector2 _pointer;

    [SerializeField] private InputActionReference movement, pointerPosition;
    
    private void OnEnable()
    {
        // Subscribe to the event
        GetComponent<Player>().speedEvent.AddListener((playerSpeed) => speed = playerSpeed);
    }

    public Rigidbody2D rb;

    
    // Update is called once per frame
    void Update()
    {
        _movement = movement.action.ReadValue<Vector2>();
        _pointer = GetPointerInput();
        transform.localScale = _pointer.x > transform.position.x ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
        
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + _movement * (speed * Time.fixedDeltaTime));
        
    }

    private Vector2 GetPointerInput()
    {
        Vector3 mousePos = pointerPosition.action.ReadValue<Vector2>();
        mousePos.z = Camera.main.nearClipPlane;
        return Camera.main.ScreenToWorldPoint(mousePos);
    }
}
