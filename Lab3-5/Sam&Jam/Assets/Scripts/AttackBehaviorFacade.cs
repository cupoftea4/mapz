﻿using UnityEngine;

public class AttackBehaviorFacade
{
    private readonly Player _player;
    private readonly BulletFactory _bulletFactory;

    public AttackBehaviorFacade(Player player, BulletFactory bulletFactory)
    {
        this._player = player;
        this._bulletFactory = bulletFactory;
    }

    public void Attack(Vector3 targetPosition)
    {
        _player.Attack();
        _player.DamageBehavior?.DealDamage(targetPosition);
    }

    public void SetDamageBehavior(IDamageBehavior damageBehavior)
    {
        _player.DamageBehavior = damageBehavior;
    }

    public void CreateBullet(Vector3 targetPosition)
    {
        _bulletFactory.CreateBullet(targetPosition);
    }
}