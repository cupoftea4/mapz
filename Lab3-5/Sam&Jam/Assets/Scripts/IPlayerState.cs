﻿public interface IPlayerState
{
    void Move();
    void Attack();
    
    int GetDuration();
}
