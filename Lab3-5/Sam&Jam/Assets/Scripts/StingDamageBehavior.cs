﻿using System.Collections;
using UnityEngine;

public class StingDamageBehavior : MonoBehaviour, IDamageBehavior
{
    private IDamageBehavior _damageBehavior;

    public StingDamageBehavior Decorate(IDamageBehavior damageBehavior)
    {
        this._damageBehavior = damageBehavior;
        return this;
    }

    public void DealDamage(Vector3 targetPosition)
    {
        _damageBehavior.DealDamage(targetPosition);
        StartCoroutine(DelayedDamage(targetPosition, 0.3f));
    }
    
    private IEnumerator DelayedDamage(Vector3 targetPosition, float delay)
    {
        yield return new WaitForSeconds(delay);

        // Execute the second damage after the specified delay
        _damageBehavior.DealDamage(targetPosition);
    }
    
}