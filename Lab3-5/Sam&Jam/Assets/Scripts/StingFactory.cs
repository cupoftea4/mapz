﻿
using UnityEngine;

public class StingFactory : BulletFactory
{
    [SerializeField] protected GameObject stingPrefab;
    public override GameObject CreateBullet(Vector3 endPoint)
    {
        var position = attackPoint.position;
        var direction = endPoint - position;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        attackPoint.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        // Instantiate the bullet with the correct rotation
        var bullet = Instantiate(stingPrefab, position, Quaternion.identity);
        bullet.transform.rotation = attackPoint.rotation;

        var rb = bullet.GetComponent<Rigidbody2D>();

        // Apply force in the direction from the attack point towards the mouse position
        rb.AddForce(direction.normalized * bulletForce, ForceMode2D.Impulse);
        return bullet;
    }

}
