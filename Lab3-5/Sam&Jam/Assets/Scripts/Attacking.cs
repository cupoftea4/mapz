using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class Attacking : MonoBehaviour
{
    private StingFactory _stingFactory;
    private AttackBehaviorFacade _attackBehaviorFacade;


    [SerializeField] private InputActionReference mousePositionAction;
    [SerializeField] private InputActionReference mouseClickAction;

    private Vector2 _mousePosition;

    private void Awake()
    {
        _attackBehaviorFacade = new AttackBehaviorFacade(
            GetComponent<Player>(),
            GetComponentInChildren<BulletFactory>()
        );
    }
    
    private void OnEnable()
    {
        mousePositionAction.action.performed += UpdateMousePosition;
        mouseClickAction.action.performed += Attack;
    }

    private void OnDisable()
    {
        mousePositionAction.action.performed -= UpdateMousePosition;
        mouseClickAction.action.performed -= Attack;
    }
    
    private void UpdateMousePosition(InputAction.CallbackContext ctx)
    {
        _mousePosition = ctx.ReadValue<Vector2>();
    }
    
    // ReSharper disable Unity.PerformanceAnalysis
    private void Attack(InputAction.CallbackContext ctx)
    {
        if (!(ctx.ReadValue<float>() > 0f)) return;
        if (Camera.main == null) return;
        var targetPosition  = Camera.main.ScreenToWorldPoint(_mousePosition);
        targetPosition .z = 0f;
    
        _attackBehaviorFacade.Attack(targetPosition);
        DetectColliders();
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 2f);
    }

    private void DetectColliders()
    {
        var colliders = Physics2D.OverlapCircleAll(transform.position, 2f);
        foreach (var collider in colliders)
        {
            if (collider.gameObject.CompareTag("Enemy"))
            {
                Debug.Log("Enemy detected" + collider.gameObject.name);
            }
        }
    }
}
