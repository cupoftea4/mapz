﻿using System.Collections.Generic;

public interface IEnemySubject
{
    void Attach(IEnemyObserver observer);
    void Detach(IEnemyObserver observer);
    void Notify();
}

public interface IEnemyObserver
{
    void UpdateEnemyDestroyed(Enemy enemy);
}
