﻿using UnityEngine;

public interface IDamageBehavior
{
    void DealDamage(Vector3 targetPosition);
}