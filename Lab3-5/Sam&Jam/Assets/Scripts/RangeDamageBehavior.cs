﻿    using UnityEngine;

    public class RangeDamageBehavior : IRangeDamage
    {
        private readonly BulletFactory _bulletFactory;

        public RangeDamageBehavior(BulletFactory bulletFactory)
        {
            this._bulletFactory = bulletFactory;
        }

        public void Shoot(Vector3 targetPosition)
        {
            // Perform range damage logic, e.g., create bullets using the bullet factory
            Debug.Log("Dealing range damage");
            _bulletFactory.CreateBullet(targetPosition);
            // ... implementation ...
        }
        
    }
