﻿using UnityEngine;

public class RangeDamageBehaviorAdapter : IDamageBehavior
{
    private readonly IRangeDamage _original;
    
    public RangeDamageBehaviorAdapter(IRangeDamage original)
    {
        _original = original;
    }
    
    // ReSharper disable Unity.PerformanceAnalysis
    public void DealDamage(Vector3 targetPosition)
    {
        _original.Shoot(targetPosition);
    }
}