﻿using System;
using System.Collections;
using UnityEngine;

public class FastState : IPlayerState
{
    private readonly Player _player;

    public FastState(Player player)
    {
        _player = player;
        _player.SetSpeed(10);
        _player.DamageBonus = 0;
    }
    

    public void Move()
    {
        Debug.Log("Player is moving fast.");
    }

    public void Attack()
    {
       
        Debug.Log("Player is attacking quickly.");
    }

    public int GetDuration()
    {
        return 5;
    }
}