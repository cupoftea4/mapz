using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum WeaponType
{
    Tentacle,
    EyeLauncher,
    StingShooter
}

public class Menu : MonoBehaviour
{
    [SerializeField] private Button tentacleButton;
    [SerializeField] private Button stingShooterButton;
    [SerializeField] private Button eyeLauncherButton;
    
    [SerializeField] private Toggle hardModeToggle;
    
    [SerializeField] private Button startButton;
    
    [SerializeField] private SamBuilder samBuilder;
    
    [SerializeField] private GameObject samPrefab;
    private void Awake()
    {
        samBuilder = gameObject.AddComponent<SamBuilder>();
        
        tentacleButton.onClick.AddListener(() => samBuilder.SetWeaponType(WeaponType.Tentacle));
        stingShooterButton.onClick.AddListener(() => samBuilder.SetWeaponType(WeaponType.StingShooter));
        eyeLauncherButton.onClick.AddListener(() =>  samBuilder.SetWeaponType(WeaponType.EyeLauncher));
        
        hardModeToggle.onValueChanged.AddListener((value) => samBuilder.SetHardMode(value));
        
        startButton.onClick.AddListener(StartGame);
        
        DontDestroyOnLoad(this);
    }

    private void StartGame()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.LoadScene("sam-n-jam");
    }
    
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "sam-n-jam")
        {
            Player sam = samBuilder.Build(samPrefab);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
