﻿using System;
using System.Collections;
using UnityEngine;

public class PowerState : IPlayerState
{
    private readonly Player _player;

    public PowerState(Player player)
    {
        _player = player;
        
    }
    

    public void Move()
    {
        Debug.Log("Player is moving normally.");
    }

    public void Attack()
    {
        _player.DamageBonus = 10;
        _player.SetSpeed(2);
        Debug.Log("Player is attacking with additional power.");
    }
    
    public int GetDuration()
    {
        return 30;
    }
    
}