﻿// Concrete player state for normal state

using System;
using UnityEngine;

public class NormalState : IPlayerState
{
    private readonly Player _player;

    public NormalState(Player player)
    {
        _player = player;
        _player.SetSpeed(5);
        _player.DamageBonus = 0;
    }

    public void Move()
    {
        Debug.Log("Player is moving normally.");
    }

    public void Attack()
    {
        Debug.Log("Player is attacking normally.");
    }

    public int GetDuration()
    {
        return 1000;
    }
}