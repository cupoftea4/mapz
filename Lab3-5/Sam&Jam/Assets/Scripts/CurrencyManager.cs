using UnityEngine;

public interface IVisitor
{
    void VisitEnemy();
}

public class CurrencyManager : MonoBehaviour, IEnemyObserver, IVisitor
{
    public static CurrencyManager Instance { get; private set; }
    
    [SerializeField] private float _currency = 0;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            
            // Prevent the singleton from being destroyed when loading new scenes
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void AddCurrency(float amount)
    {
        _currency += amount;
    }
    
    public void RemoveCurrency(int amount)
    {
        _currency -= amount;
    }
    
    public float GetCurrency()
    {
        return _currency;
    }

    public void UpdateEnemyDestroyed(Enemy enemy)
    {
        AddCurrency(5);
    }

    public void VisitEnemy()
    {
        AddCurrency(0.5f);
    }
}