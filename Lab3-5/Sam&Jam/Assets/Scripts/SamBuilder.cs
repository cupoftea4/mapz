﻿using UnityEngine;

public class SamBuilder : MonoBehaviour
{
    private WeaponType _weaponType;
    
    private bool _isHardMode;
    
    public void SetWeaponType(WeaponType weaponType)
    {
        _weaponType = weaponType;
    }
    
    public void SetHardMode(bool isHardMode)
    {
        _isHardMode = isHardMode;
    }
    
    public Player Build(GameObject samPrefab)
    {
        var instantiatedPrefab = Instantiate(samPrefab);
        var attackPointTransform = instantiatedPrefab.transform.Find("AttackPoint");
        var attackPointObject = attackPointTransform.gameObject;

        var sam = instantiatedPrefab.GetComponent<Player>();
        
        var health = _isHardMode ? 5 : 10;
        sam.SetStats(health, 5);
        sam.SetWeaponType(_weaponType);

        
        IDamageBehavior damageBehavior = _weaponType switch
        {
            WeaponType.Tentacle => new RangeDamageBehaviorAdapter(
                new RangeDamageBehavior(sam.GetComponentInChildren<BulletFactory>())),
            WeaponType.StingShooter => 
                attackPointObject.AddComponent<StingDamageBehavior>().Decorate(
                    new RangeDamageBehaviorAdapter(
                    new RangeDamageBehavior(sam.GetComponentInChildren<StingFactory>())
                    )),
            WeaponType.EyeLauncher => new RangeDamageBehaviorAdapter(
                new RangeDamageBehavior(sam.GetComponentInChildren<EyeFactory>())
                ),
            _ => new RangeDamageBehaviorAdapter(
                new RangeDamageBehavior(sam.GetComponentInChildren<BulletFactory>()))
        };
        
        sam.DamageBehavior = damageBehavior;
        return sam;
    }
}
