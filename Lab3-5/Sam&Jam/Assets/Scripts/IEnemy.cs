﻿public interface IEnemy
{
    void Attack();
    void TakeDamage(int amount);
}