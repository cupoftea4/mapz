﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyGroup : MonoBehaviour, IEnemy, IEnemyObserver
{
    private readonly List<IEnemy> _enemies = new();

    private void Start()
    {
        foreach (var enemy in GetComponentsInChildren<Enemy>())
        {
            AddEnemy(enemy);
            enemy.Attach(this);
            enemy.Attach(CurrencyManager.Instance);
        }
    }
    
    public void UpdateEnemyDestroyed(Enemy enemy)
    {
        RemoveEnemy(enemy);
    }

    private void FixedUpdate()
    {
        Attack();
    }

    private void AddEnemy(IEnemy enemy)
    {
        _enemies.Add(enemy);
    }

    private void RemoveEnemy(IEnemy enemy)
    {
        _enemies.Remove(enemy);
    }

    public void Attack()
    {
        IAggregate<IEnemy> aggregate = new GeneralAggregate<IEnemy>(_enemies.ToArray());
        var iterator = aggregate.CreateIterator();
        while (iterator.HasNext())
        {
            var enemy = iterator.Next();
            enemy.Attack();
        }
    }

    public void TakeDamage(int amount)
    {
        foreach (var enemy in _enemies)
        {
            enemy.TakeDamage(amount);
        }
    }
}