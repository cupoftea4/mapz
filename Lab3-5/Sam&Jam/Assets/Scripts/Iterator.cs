﻿using UnityEngine;
using System.Collections;

// The Iterator interface defines the operations for iterating over a collection
public interface IIterator<T>
{
    bool HasNext();
    T Next();
}

// The Aggregate interface defines the methods to create an Iterator
public interface IAggregate<T>
{
    IIterator<T> CreateIterator();
}

// Concrete Iterator implementation for Unity objects
public class GeneralIterator<T> : IIterator<T> 
{
    private readonly T[] _objects;
    private int _currentIndex;

    public GeneralIterator(T[] objects)
    {
        this._objects = objects;
        _currentIndex = 0;
    }

    public bool HasNext()
    {
        return _currentIndex < _objects.Length;
    }

    public T Next()
    {
        var currentObject = _objects[_currentIndex];
        _currentIndex++;
        return currentObject;
    }
}

// Concrete Aggregate implementation for Unity objects
public class GeneralAggregate<T> : IAggregate<T> 
{
    private readonly T[] _objects;

    public GeneralAggregate(T[] objects)
    {
        this._objects = objects;
    }

    public IIterator<T> CreateIterator()
    {
        return new GeneralIterator<T>(_objects);
    }
    
}