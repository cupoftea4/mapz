﻿public class Memento
{
    public IPlayerState State { get; }

    public Memento(IPlayerState state)
    {
        this.State = state;
    }
}