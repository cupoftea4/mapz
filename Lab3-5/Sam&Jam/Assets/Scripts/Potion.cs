﻿using System;
using System.Collections;
using UnityEngine;

public class Potion : MonoBehaviour
{
    [SerializeField] private string type;
    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("POTION: Potion collided with " + other.gameObject.name);
        if (!other.gameObject.CompareTag("Player")) return;
        var player = other.gameObject.GetComponent<Player>();
        if (player == null) return;
        var memento = player.CreateMemento();
        
        var duration = ApplyPotionEffect(player);
        
        Destroy(gameObject);

        Debug.Log("Player picked up a potion!");

        StartCoroutine(RestorePlayerState(player, memento, duration));
    }

    private int ApplyPotionEffect(Player player)
    {
        IPlayerState state = type switch {
            "power" => new PowerState(player),
            "speed" => new FastState(player),
            _ => throw new ArgumentOutOfRangeException()
        };
        player.SetState(state);
        return state.GetDuration();
    }

    private IEnumerator RestorePlayerState(Player player, Memento memento, int time)
    {
        yield return new WaitForSeconds(time);
        
        player.RestoreMemento(memento);
        Debug.Log("Player's state restored.");
    }
}