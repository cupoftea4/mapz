using System.Collections.Generic;
using System.Linq;
using UnityEngine;

internal interface IAcceptor
{
    void Accept(IVisitor visitor);
}

public class Enemy : MonoBehaviour, IEnemy, IEnemySubject, IAcceptor
{
    [SerializeField] private int health = 3;
    
    private Transform _playerTransform;
    private const float MovementSpeed = .05f;
    private Collider2D _enemyCollider;
    private IEnumerable<Collider2D> _enemyColliders;
    [SerializeField] private float stoppingDistance = 2.0f;     // Distance at which the enemy stops moving towards the player
    [SerializeField] private float avoidanceForce = 5.0f;
    
    private readonly List<IEnemyObserver> _observers = new();
    private IVisitor _visitor;

    void Start()
    {
        _enemyCollider = GetComponent<Collider2D>();
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        _enemyColliders = GameObject.FindGameObjectsWithTag("Enemy")
            .Select(obj => obj.gameObject.GetComponent<Collider2D>())
            .Where(collider => collider != null)
            .ToList();
        Accept(CurrencyManager.Instance);
        
    }
    
    

    public void Attack()
    {
        MoveTowardsPlayer();
    }
    
    private void MoveTowardsPlayer()
    {
        if (gameObject == null) return;
        
        var direction = _playerTransform.position - transform.position;
        var movement = direction.normalized * MovementSpeed;
        
        var distanceToPlayer = direction.magnitude;
        if (distanceToPlayer > stoppingDistance)
        {
            transform.Translate(movement);
        }

        if (_enemyColliders == null || !_enemyColliders.Any()) return;

        foreach (var enemyCollider in _enemyColliders)
        {
            if (enemyCollider == null || enemyCollider == _enemyCollider || !enemyCollider.CompareTag("Enemy"))
                continue;
            
            var position = transform.position;
            var avoidanceDirection = position - enemyCollider.transform.position;
            Debug.DrawRay(position, avoidanceDirection, Color.red);
            transform.Translate(avoidanceDirection.normalized * (avoidanceForce * 0.001f));
        }
    }
    
    public void TakeDamage(int damage)
    {
        _visitor?.VisitEnemy();
        health -= damage;
        if (health > 0) return;
        CurrencyManager.Instance.AddCurrency(1);
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        UpdateEnemyDestroyed();
        Notify();
    }

    public void Attach(IEnemyObserver observer)
    {
        _observers.Add(observer);
    }
    
    public void Detach(IEnemyObserver observer)
    {
        _observers.Remove(observer);
    }
    
    public void Notify()
    {
        foreach (var observer in _observers)
        {
            observer.UpdateEnemyDestroyed(this);
        }
    }

    private void UpdateEnemyDestroyed()
    {
        var component = this.GetComponent<Collider2D>();
        
        if (component == null || component.gameObject == null) return;
        
        _enemyColliders = _enemyColliders.Where(c => c != component);
    }


    public void Accept(IVisitor visitor)
    {
        _visitor = visitor;
    }
}
