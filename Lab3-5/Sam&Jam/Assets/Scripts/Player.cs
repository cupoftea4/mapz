﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] private int health = 3;
    [SerializeField] private int speed = 5;
    public UnityEvent<int> speedEvent;
    public int DamageBonus { get; set; }

    [SerializeField] private Animator animator;
    private IPlayerState _currentState;

    private WeaponType _weaponType;
    private static readonly int Form = Animator.StringToHash("Form");
    private static readonly int AttackTrigger = Animator.StringToHash("Attack");
    public IDamageBehavior DamageBehavior { get; set; }

    private void Start()
    {
        DamageBonus = 0;
        _currentState = new NormalState(this);
    }

    public void Attack()
    {
        _currentState.Attack();
        Debug.Log("Attack");
        animator.SetTrigger(AttackTrigger);
    }


    public void SetStats(int health, int speed)
    {
        this.health = health;
        SetSpeed(speed);
    }
    
    public void SetSpeed(int speed)
    {
        this.speed = speed;
        this.speedEvent.Invoke(speed);
    }
    
    public void SetWeaponType(WeaponType weaponType)
    {
        _weaponType = weaponType;
        Debug.Log(_weaponType);
        
        animator.SetInteger(Form, (int)_weaponType + 1);
    }
    
    public void SetState(IPlayerState state)
    {
        _currentState = state;
    }
    

    public Memento CreateMemento()
    {
        return new Memento(_currentState);
    }
    
    public void RestoreMemento(Memento memento)
    {
        _currentState = memento.State;
    }
}
