﻿using UnityEngine;

public class EyeBullet : Bullet
{
    [SerializeField] private GameObject explosionPrefab;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var enemy = collision.gameObject.GetComponent<Enemy>();
        enemy.TakeDamage(2);
        
        var explosion = Instantiate(explosionPrefab, collision.contacts[0].point, Quaternion.identity);
        
        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        
        
       Destroy(explosion, .5f);
       StartCoroutine(DestroyAfterDelay(gameObject, .6f));
    }
}