﻿namespace Lab2;

internal static class ModelTests
{
    public static readonly List<FoxSpecies> Species = new()
        {
            new FoxSpecies(VulpesSpeciesName.ArcticFox),
            new FoxSpecies(VulpesSpeciesName.RedFox),
            new FoxSpecies(VulpesSpeciesName.FennecFox),
            new FoxSpecies(VulpesSpeciesName.KitFox),
            new FoxSpecies(VulpesSpeciesName.SwiftFox),
            new FoxSpecies("Gray Fox")
        };
    
    public static Guid GetSpeciesId(VulpesSpeciesName name) =>
        Species.Find(el => el.Name == name.ToString())?.Id ?? Guid.Empty;

    public static List<Fox?> Foxes =>
        new()
        {
            new Fox("Buttercup", Species[5]) { Age = 5 },
            new Fox("Mathew", Species[1]) { Age = 7 },
            new Fox("Astra", Species[2]) { Age = 1 },
            new Fox("Elm", Species[4]) { Age = 3 },
            new Fox("Hazelnut", Species[3]) { Age = 2 },
            new Fox("Lucky", Species[2]) { Age = 6 },
            new Fox("Misty", Species[5]) { Age = 8 },
            new Fox("Chomp", Species[0]) { Age = 1 },
            new Fox("Crimson", Species[0]) { Age = 5 },
            new Fox("Celia", Species[3]) { Age = 10 },
            new Fox("Kitsune", Species[3]) { Age = 418 },
        };
}