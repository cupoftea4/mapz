﻿namespace Lab2;

internal class Fox
{
    public Guid Id { get; } = Guid.NewGuid();
    public Guid VulpesSpeciesId { get; }
    public string Name { get; set; }
    public int Age { get; set; }

    public readonly bool IsTrue = false;

    public Fox(string name, FoxSpecies species, int age = 1)
    {
        VulpesSpeciesId = species.Id;
        IsTrue = Enum.TryParse(species.Name, out VulpesSpeciesName res);
        Name = name;
        Age = age;
    }

    public void GrowOlder()
    {
        Age++;
    }
}

internal class FoxesComparator : IComparer<Fox>
{
    public int Compare(Fox? a, Fox? b)
    {
        return (a?.Name == "" || b?.Name == ""
            ? a?.Id.CompareTo(b?.Id)
            : String.Compare(a?.Name, b?.Name, StringComparison.Ordinal)) ?? 0;
    }
}

internal class FoxSpecies
{
    public FoxSpecies(VulpesSpeciesName name)
    {
        Id = Guid.NewGuid();
        Name = name.ToString();
    }
    
    public FoxSpecies(string name)
    {
        Id = Guid.NewGuid();
        Name = name;
    }

    public string Name { get; set; }
    public Guid Id { get; set; }
}

internal enum VulpesSpeciesName
{
    ArcticFox,
    RedFox,
    FennecFox,
    KitFox,
    SwiftFox
}