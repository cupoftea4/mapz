﻿
namespace ListExtensions
{
    public static class ListExtensions
    {
        public static SortedList<K, T> ToSortedList<K, T>(this List<T> list, Func<T, K> keySelector) where K : notnull
        {

            SortedList<K, T> sortedList = new();
            list.ForEach(el => sortedList.Add(keySelector(el), el));
            return sortedList;
        }
    }
}