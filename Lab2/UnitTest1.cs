using System.Diagnostics;

namespace Lab2;
using ListExtensions;



public static class Tests
{
    private static List<FoxSpecies> FoxSpecies => ModelTests.Species;
    private static List<Fox> Foxes => ModelTests.Foxes;
    
    [SetUp]
    public static void Setup()
    {
        ShowFoxes();
    }

    [Test]
    public static void TestFoxesDictionaryByCategory()
    {
        var foxesByCategory = FoxSpecies.ToDictionary(
            (species) => species.Id,
            (species) => Foxes.FindAll(el => el.VulpesSpeciesId == species.Id)
        );
        Assert.That(foxesByCategory.Count, Is.EqualTo(FoxSpecies.Count));
    }
    
    [Test]
    public static void TestDictionaryOperations()
    {
        var foxesByCategory = Foxes.ToDictionary(
            (fox) => fox.Id,
            (fox) => fox
        );
        var result = foxesByCategory.Reverse()
            .AsParallel().WithDegreeOfParallelism(3)
            .Where(el => el.Value.Age > 5);
        Assert.That(result.Count, Is.EqualTo(5));
    }

    [Test]
    public static void TestFoxesDictionary()
    {
        var foxesMap = Foxes.ToDictionary(
            (fox) => fox.Id,
            (fox) => fox
        );
        Assert.That(foxesMap.Keys, Is.All.TypeOf<Guid>());
    }
    
    [Test]
    public static void TestFoxesSortedList()
    {
        var foxesSortedList = Foxes.ToSortedList(fox => fox.Name); // extension method
        Assert.That(foxesSortedList.Keys[0], Is.EqualTo("Astra"));
    }
    
    [Test]
    public static void TestFoxesQueue()
    {
        var foxesQueue = new Queue<Fox>(Foxes);
        Assert.That(foxesQueue.Peek().Name, Is.EqualTo("Buttercup"));
    }

    [Test]
    public static void TestFoxesStack()
    {
                
        Stack<Fox> foxesStack = new(Foxes);
        while (foxesStack.Count > 0)
        {
            foxesStack.Pop();
        }
        Assert.That(foxesStack, Is.Empty);
    }
    
    [Test]
    public static void TestFoxesHashSet()
    {
        var foxesHashSet = new HashSet<Fox>(Foxes);
        Assert.That(foxesHashSet.Count, Is.EqualTo(Foxes.Count));
    }
    
    [Test]
    public static void TestFoxesLookup()
    {
        var foxesLookup = Foxes.ToLookup(fox => fox.VulpesSpeciesId);
        
        var arcticFox = foxesLookup[FoxSpecies[0].Id].First();
        Assert.That(arcticFox.Name, Is.EqualTo("Chomp")); // first arctic fox

        var fennecFoxes = 
            foxesLookup[ModelTests.GetSpeciesId(VulpesSpeciesName.FennecFox)];
        Assert.That(fennecFoxes.Count(), Is.EqualTo(2));
        
        var count = foxesLookup.Sum(group => group.Count());
        Assert.That(count, Is.EqualTo(Foxes.Count));
    }

    [Test]
    public static void TestSort()
    {
        var orderedEnumerable = Foxes.OrderBy(_ => _, new FoxesComparator());
        Assert.That(
            orderedEnumerable.Select(fox => fox.Name), 
            Is.EqualTo(Foxes.Select(fox => fox.Name).OrderBy(name => name))
        );
    }
    
    [Test]
    public static void TestGroupBy()
    {
        var grouped = Foxes.GroupBy(fox => fox.VulpesSpeciesId);
        Assert.That(grouped.Count(), Is.EqualTo(FoxSpecies.Count));
    }
    
    [Test]
    public static void TestComplicatedOperations()
    {
        var sortedSpeciesByFirstFoxName = FoxSpecies
            .Select(species => (
                species, 
                Foxes.Where(fox => fox.VulpesSpeciesId == species.Id).Select(fox => fox.Name).First()
                ))
            .OrderByDescending(pair => pair.Item2)
            .Select(pair => pair.species);

        Assert.That(
            sortedSpeciesByFirstFoxName.First().Name, 
            Is.EqualTo(VulpesSpeciesName.RedFox.ToString()));
    }
    
    [Test]
    public static void TestAnonymousType()
    {
        var foxes = Foxes.Select(fox => new { fox.Name, fox.VulpesSpeciesId });
        Assert.That(foxes.Count(), Is.EqualTo(Foxes.Count));
    }
    
    [Test] 
    [TestCase(1, 500)]
    public static void TestToArray(int min, int max)
    {
        var arr = Foxes.ToArray().Select(fox => fox.Age).ToArray();
        Assert.That(arr, Is.All.InRange(min, max));
    }

    [Test]
    public static void TestAggregate()
    {
        var maxAge = Foxes
            .Select(fox => fox.Age)
            .Aggregate(10, (max, el) => el > max ? el : max);
        Assert.That(maxAge, Is.EqualTo(418));
    }

    [Test]
    public static void TestLastOrDefault()
    {
        var lastTrueFox = Foxes.LastOrDefault(fox => fox.IsTrue);
        Assert.That(lastTrueFox, Is.Not.Null);
    }

    private static void ShowFoxes()
    {
        var sb = new System.Text.StringBuilder();
        sb.Append(String.Format(">{0,9} {1,40} {2,15} {3, 5} {4, 6}\n", "Name", "Species ID", "Species Name", "Age", "IsTrue"));
        Foxes.ForEach(fox =>
            sb.Append(String.Format("{0,10} {1,40} {2,15} {3, 5} {4, 15}\n",   
                fox.Name,   
                fox.VulpesSpeciesId, 
                FoxSpecies.Find(el => el.Id == fox.VulpesSpeciesId)?.Name.ToString() ?? "Unknown",
                fox.Age.ToString(),
                fox.IsTrue.ToString()
                )
            ));
        Console.WriteLine(sb);
    }
}