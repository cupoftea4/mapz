﻿namespace Lab01;


internal static class Ref
{
    public static void AddOne(ref int val) => val++;

    public static void SetCatField(DomesticCat cat, string name) =>
        cat.Name = name;
}

internal static class Out
{
    public static void GetRandomValues(out int val1, out int val2)
    {
        var randomizer = new Random();
        val1 = randomizer.Next();
        val2 = randomizer.Next();
    }
}