﻿using System.Diagnostics;
using Lab01;

// Task1.TestConstructors();
// Task1.TestEnums();
// Task1.TestRefAndOut();
// Task1.TestBoxingAndUnboxing();
// Task1.TestImplicitAndExplicitCasting();
// Task1.TestCatStructs();
// Task1.TestObjectMethods();
//
// Task2.MeasureCreationTime();
// Task2.MeasureMethodCallTime();
// Task2.MeasureArithmeticOperations();

// IndividualVar.CallStaticMethods();
IndividualVar.CallDynamicMethods();
// Console.ReadLine();

internal static class Task1
{
    private static readonly DomesticCat Fluffy = new DomesticCat("Fluffy");
    
    public static void TestConstructors()
    {
        Fluffy.Eat("Ice-cream");
        Fluffy.Meow();

        new DomesticCat("Nice");
    }
    
    public static void TestEnums()
    {
        Console.WriteLine("\n ---Enums bitwise operators---");
        const CatColor newSecretColor = CatColor.White & CatColor.Black;
        Console.WriteLine("{1}'s color: {0}", newSecretColor.ToString(), Fluffy.Name);
    }
    
    public static void TestRefAndOut()
    {
        Console.WriteLine("\n ---Ref and out---");
        var two = 2;
        Ref.AddOne(ref two);
        Console.WriteLine($"Adding one to number 2: {two}");
        
        var name = "New cat name";
        Ref.SetCatField(Fluffy, name);
        Console.WriteLine($"Cat name: {Fluffy.Name}. String: {name}");

        Out.GetRandomValues(out var returnValue1, out var returnValue2);
        Console.WriteLine($"Random values {returnValue1}, {returnValue2}");
    }
    
    public static void TestBoxingAndUnboxing()
    {
        Console.WriteLine("\n ---Boxing and unboxing---");
        var number = 42;
        object obj = number;
        Console.WriteLine($"Boxed value: {obj}");

        obj = 720;
        number = (int)obj; 
        Console.WriteLine($"Unboxed value: {number}");
    }
    
    public static void TestImplicitAndExplicitCasting()
    {
        Console.WriteLine("\n ---Implicit and explicit casting---");
        string cat = Fluffy;
        var catNumber = (int) Fluffy;
        Console.WriteLine($"Implicit casting: {cat} {catNumber}");
        
        var number = (int) Fluffy;
        Console.WriteLine($"Explicit casting: {number}");
    }

    public static void TestCatStructs()
    {
        Console.WriteLine("\n ---Domestic cat using structs---");
        var cat = new SDomesticCat("Furball");
        Console.WriteLine($"Name property: {cat.Name}");
        cat.Eat("salad -.-");
        cat.Meow();
    }

    public static void TestObjectMethods()
    {
        Console.WriteLine("\n ---Object overriden methods---");
        var cat = new DomesticCat("Frisbee");
        Console.WriteLine($"{cat} {cat.GetHashCode()} {cat.Equals(Fluffy)}");
    }
}


internal static class Task2
{
    private static readonly DomesticCat ClassCat = new DomesticCat("Class Fluffy");
    private static readonly SDomesticCat StructCat = new SDomesticCat("Struct Fluffy");

    static Task2()
    {
        Measurement.RunTimes = 10_000_000;
    }

    public static void MeasureCreationTime()
    {
        var classesTime = Measurement.Run(() => new CatOwner());
        var structsTime = Measurement.Run(() => new SDomesticCat());
        var derivedClassesTime = Measurement.Run(() => new UkrainianDomesticCat());
        Console.WriteLine($"Milliseconds elapsed for classes: {classesTime}. " +
                          $"For structs: {structsTime}. " +
                          $"For derived classes: {derivedClassesTime}");
    }
    
    public static void MeasureMethodCallTime()
    {
        var classesTime = Measurement.Run(() => ClassCat.DoSomething());
        var structsTime = Measurement.Run(() => StructCat.DoSomething());
        Console.WriteLine($"Milliseconds elapsed for classes: {classesTime}. " +
                          $"For structs: {structsTime}");
    }

    public static void MeasureArithmeticOperations()
    {
        var classesTime = Measurement.Run(() => ClassCat.DoCatMath());
        var structsTime = Measurement.Run(() => StructCat.DoCatMath());
        Console.WriteLine($"Milliseconds elapsed for classes: {classesTime}. " +
                          $"For structs: {structsTime}");
    }
    
}

internal static class Measurement
{
    public static long RunTimes = 10_000_000; 
    public delegate void Del();

    public static long Run(Del task)
    {
        var timer = Stopwatch.StartNew();
        for (var i = 0; i < RunTimes; i++)
        {
            task();
        }
        return timer.ElapsedMilliseconds;
    }
}

internal static class IndividualVar
{
    private static readonly CatOwner Owner = new CatOwner();
    private static void CallStaticMethod()
    {
        CatHelper.IsKitten(2);
    }

    private static void CallDynamicMethod()
    {
        Owner.IsHappy(2);
    }
    
    private static void StaticProperty()
    {
        CatHelper.DefaultName = "Test";
    }
    
    private static void DynamicProperty()
    {
        Owner.Name = "Test";
    }
    
    private static string AccessStaticProperty()
    {
        return CatHelper.DefaultName;
    }
    
    private static string AccessDynamicProperty()
    {
        return Owner.Name;
    }

    public static void MeasureCallMethods()
    {
        var dynamicTime = Measurement.Run(CallDynamicMethod);
        var staticTime = Measurement.Run(CallStaticMethod);
        Console.WriteLine($"Milliseconds elapsed for static class: {staticTime}. " +
                          $"For dynamic class: {dynamicTime}");
    }

    public static void MeasureProperties()
    {
        var dynamicTime = Measurement.Run(DynamicProperty);
        var staticTime = Measurement.Run(StaticProperty);
        Console.WriteLine($"Milliseconds elapsed for static class: {staticTime}. " +
                          $"For dynamic class: {dynamicTime}");
    }
    
    public static void MeasureAccessToProperties()
    {
        var dynamicTime = Measurement.Run(() => AccessDynamicProperty());
        var staticTime = Measurement.Run(() => AccessStaticProperty());
        Console.WriteLine($"Milliseconds elapsed for static class: {staticTime}. " +
                          $"For dynamic class: {dynamicTime}");
    }

    public static void CallStaticMethods()
    {
        var staticTime = Measurement.Run(CallStaticMethod);
        Console.WriteLine($"Milliseconds elapsed for static class: {staticTime}");
    }
    
    public static void CallDynamicMethods()
    {
        var dynamicTime = Measurement.Run(CallDynamicMethod);
        Console.WriteLine($"Milliseconds elapsed for dynamic class: {dynamicTime}");
    }
}