﻿
namespace Lab01;

internal interface ISAlive
{
    protected void Eat();
}

// Немає статичних конструкторів у структурах
// Немає наслідування від структур
// Немає абстрактних стркутур
internal struct SDomesticCat :  IAnimal, ISAlive
{
    private string _owner;
    private const string WhatDoesTheCatSay = "Yay";
    private const int Whiskers = 8;

    public string Name { get; }

    public SDomesticCat(string name, string owner = "Amy")
    {
        _owner = owner;
        Name = name;
    }

    public SDomesticCat() : this("Noodle", "Amy1") {}
    
    public void Eat()
    {
        Console.WriteLine($"{Name} is eating cat food.");
    }
    
    public void Eat(string foodName)
    {
        Console.WriteLine($"{Name} is eating {foodName}.");
    }

    public void Meow()
    {
        Console.WriteLine($"{Name} says: {WhatDoesTheCatSay}!");
    }
    
    public readonly int DoSomething()
    {
        return Name.Length * 69;
    }
    
    public readonly double DoCatMath()
    {
        const int whiskerSquares = Whiskers * Whiskers;
        var catRoot = Math.Sqrt(Whiskers);
        const int sleepSheep = 42;
        var catsLifeMeaning = sleepSheep / catRoot - whiskerSquares;
        return catsLifeMeaning;
    }
}

// internal struct SCat
// {
//     private readonly string Name;
//     
//     public SCat(string name)
//     {
//         Name = name;
//     }
//
//     // public abstract void Meow();
//     
//     public static implicit operator string(SCat cat) => cat.Name;
// }

// internal class SUkrainianDomesticCat : ISAlive
// {
//     public SUkrainianDomesticCat(string name, string owner) : base(name, owner) { }
//
//     public void UkrainianMeow()
//     {
//         Console.WriteLine($"{Name} says: Мяу!");
//     }
// }