﻿namespace Lab01;

internal interface IAlive
{
    protected void Eat();
}

internal abstract class Cat
{
    public string Name { get; set; }
    protected readonly int Age;
    
    protected Cat(string name)
    {
        Name = name;
        this.Age = 0;
    }

    public abstract void Meow();
    
    public static implicit operator string(Cat cat) => cat.Name;
    public static explicit operator int(Cat cat) => cat.Name.Length;
}

internal class DomesticCat : Cat, IAlive
{
    private static readonly int Whiskers = 8;
    private readonly string _owner;
    private static readonly string WhatDoesTheCatSay;

    // public string GetName => Name;

    static DomesticCat()
    {
        // Console.WriteLine("Static Constructor");
        WhatDoesTheCatSay = "MEOW";
    }

    public DomesticCat(string name, string owner = "Amy") : base(name)
    {
        // Console.WriteLine("Dynamic Constructor");
        _owner = owner;
    }

    protected DomesticCat() : this("Noodle", "Amy1") {}
    
    public void Eat()
    {
        Console.WriteLine($"{Name} is eating cat food.");
    }
    
    public void Eat(string foodName)
    {
        Console.WriteLine($"{Name} is eating {foodName}.");
    }

    public override void Meow()
    {
        Console.WriteLine($"{Name} says: {WhatDoesTheCatSay}!");
    }

    public override string ToString()
    {
        return $"{Name} is a domestic cat owned by {_owner} and is {Age} y.o.";
    }

    public override bool Equals(object? obj)
    {
        return obj is DomesticCat cat && Name == cat.Name;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Age);
    }

    public int DoSomething()
    {
        return Name.Length * 69;
    }
    

    public double DoCatMath()
    {
        var whiskerSquares = Whiskers * Whiskers;
        var catRoot = Math.Sqrt(Whiskers);
        const int sleepSheep = 42;
        var catsLifeMeaning = sleepSheep / catRoot - whiskerSquares;
        return catsLifeMeaning;
    }
}

internal class UkrainianDomesticCat : DomesticCat
{
    public UkrainianDomesticCat(string name, string owner) : base(name, owner) { }

    public void UkrainianMeow()
    {
        Console.WriteLine($"{Name} says: Мяу!");
    }
    
    public UkrainianDomesticCat() : base() {}
}

internal static class CatHelper {
    public static string DefaultName { get; set; } = "Murka";
    public static void Purr() {
        Console.WriteLine("Purr...");
    }

    public static string NameMeow(string name) {
        return "Meow! My name is " + name;
    }

    public static bool IsKitten(int age) {
        return (age < 1);
    }
}

internal class CatOwner
{
    public string Name { get; set; }
    private readonly int _catCount;
    public CatOwner(string name = "Whoever wants to own a cat", int catCount = 2)
    {
        _catCount = catCount;
        Name = name;
    }

    public string PetCat(string catName)
    {
        return Name + " pets " + catName;
    }

    public bool IsHappy(int catCount)
    {
        return (_catCount > catCount);
    }
    
}