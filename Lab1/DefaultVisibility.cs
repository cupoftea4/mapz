﻿namespace Lab01;

struct SDefaultCat: IAnimal
{
    string _name = "";
    string Name { get; }

    SDefaultCat(string name) => this._name = name;
}

interface IAnimal
{
    void Eat() {}
}

class DefaultCat: IAnimal
{
    CatToy toy;
    
    public void Eat() => Console.WriteLine("Cat is eating");
    void SayMeow() => Console.WriteLine("Meow");

    DefaultCat()
    {
        toy = new CatToy();
    }
    
    class CatToy
    {
        int _size;

        public CatToy(bool isBig = false)
        {
            this._size = isBig ? 10 : 5;
        }
    }
}