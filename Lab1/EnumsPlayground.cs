﻿namespace Lab01;

[Flags]
internal enum CatColor
{
    NotVisible = 0,
    Orange = 10,
    White = 1,
    Black = 2,
    BlackAndWhite = White | Black,
    AlmostGray = White << Black,
    Alien = Orange >> White,
    Tasty = Orange ^ Black,
    NotBlack = ~Black,
}
